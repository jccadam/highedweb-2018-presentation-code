export default function fetchJson(url) {
  return fetch(url).then(resp => {
    return resp.json();
  }).then(json => {
    if (!(json instanceof Object)) return;
    return json;
  });
}
