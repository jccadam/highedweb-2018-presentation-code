import fetchJson from '../fetchJson';
export const FETCH_SEARCH_RESULTS = 'FETCH_SEARCH_RESULTS';
export const RECEIVE_SEARCH_RESULTS = 'RECEIVE_SEARCH_RESULTS';

export function fetchResults(query) {
  return dispatch => {
    dispatch(fetchSearchResults(query));

    if (query === '')
      return dispatch(receiveSearchResults([]));

    return fetchJson(
      Drupal.url(`school_search/results/${query}`)
    ).then(json => {
      if (!(json['results'] instanceof Array)) return;
      dispatch(receiveSearchResults(json['results']));
    });
  };
}

export const fetchSearchResults = query => ({
  type: FETCH_SEARCH_RESULTS,
  query
});

export const receiveSearchResults = results => ({
  type: RECEIVE_SEARCH_RESULTS,
  results
});
