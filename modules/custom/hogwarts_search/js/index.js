import React from 'react';
import ReactDOM from 'react-dom';
import thunkMiddleware from 'redux-thunk';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import SearchInput from './containers/SearchInput';
import searchReducer from './reducers/searchReducers';

window.addEventListener('DOMContentLoaded', () => {
  const searchEl = document.getElementById('autocompleteSearch');
  if (!searchEl) return;

  const reducers = combineReducers({
    search: searchReducer
  });

  const store = createStore(
    reducers,
    applyMiddleware(thunkMiddleware)
  );

  ReactDOM.render(
    <Provider store={store}>
      <SearchInput />
    </Provider>,
    searchEl
  );
});
