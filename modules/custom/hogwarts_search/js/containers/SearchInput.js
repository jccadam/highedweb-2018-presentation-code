import React from 'react';
import PropTypes from 'prop-types';
import Autocomplete from 'react-autocomplete';
import { connect } from 'react-redux';
import { capitalize } from 'inflection';
import { fetchResults } from '../actions/searchActions';

class SearchInputComponent extends React.Component {
  static propTypes = {
    results: PropTypes.array.isRequired,
    query: PropTypes.string.isRequired,
    onInputChange: PropTypes.func.isRequired
  }

  _queryUrl = () => `school_search/search/${this.props.query}`

  _renderResult (result, isHighlighted) {
    const { query } = this.props;
    let renderedResult;
    let classNames = ['result'];
    if (isHighlighted) classNames.push('highlighted');

    if (result.type === '_search' && query.length > 0) {
      renderedResult = (
        <a href={Drupal.url(this._queryUrl())}>
          Search Hogwarts for "{query}"
        </a>
      );
    } else if (result.type !== '_search') {
      renderedResult = (
        <a href={Drupal.url(result.link)} className="result-link">
          <div className="result-title">
            {result.title}
          </div>
          <div className="result-type">
            <span>{capitalize(result.type)}</span>
          </div>
        </a>
      );
    }

    return (
      <div className={classNames.join(' ')} key={result.nid}>
        {renderedResult}
      </div>
    );
  }

  _renderMenu (items, value, styles) {
    return <div className="autocomplete-results" children={items} />;
  }
  
  render () {
    const { query, results, onInputChange } = this.props;
    // add a dummy item to account for the "Search Hogwarts..." option
    const items = [
      ...results,
      {nid: -1, type: '_search', link: this._queryUrl()}
    ];

    return (
      <div className="search-container">
        <label id="searchLabel">Start typing to search</label>
        <div className="search-autocomplete">
          <Autocomplete
            getItemValue={item => item.link}
            items={items}
            value={query}
            inputProps={{['aria-labelledby']: 'searchLabel'}}
            renderItem={this._renderResult.bind(this)}
            renderMenu={this._renderMenu.bind(this)}
            onChange={e => onInputChange(e.target.value)}
            onSelect={val => window.location = Drupal.url(val)} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ search }, ownProps) => ({
  query: search.query,
  results: search.results
});

const mapDispatchToProps = {
  onInputChange: fetchResults
};

const SearchInput = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchInputComponent);

export default SearchInput;
