import {
  FETCH_SEARCH_RESULTS,
  RECEIVE_SEARCH_RESULTS
} from '../actions/searchActions';

const defaultState = {
  isFetching: false,
  query: '',
  results: []
};

const reducers = {
  [FETCH_SEARCH_RESULTS](state, { query }) {
    return { ...state, isFetching: true, query };
  },

  [RECEIVE_SEARCH_RESULTS](state, { results }) {
    return { ...state, isFetching: false, results };
  }
};

export default function sliderReducer(state = defaultState, action) {
  const { type } = action;
  if (reducers.hasOwnProperty(type)) return reducers[type](state, action);
  return state;
}
