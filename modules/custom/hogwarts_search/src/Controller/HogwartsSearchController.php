<?php

namespace Drupal\hogwarts_search\Controller;

use Drupal\Core\Controller\ControllerBase;

class HogwartsSearchController extends ControllerBase {
  public function schoolSearch() {
    return [
      '#theme' => 'hogwarts_search',
      '#attached' => [
        'library' => ['hogwarts_search/hogwarts_search.school_search']
      ]
    ];
  }

  public function results($query) {
    return [
      '#theme' => 'results',
      '#query' => $query
    ];
  }
}
