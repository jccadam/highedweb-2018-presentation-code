<?php

namespace Drupal\hogwarts_search\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * The resource that provides instant search results
 * 
 * @RestResource(
 *   id = "search_results_resource",
 *   label = @Translation("Search Results Resource"),
 *   uri_paths = {
 *     "canonical" = "/school_search/results/{query}"
 *   }
 * )
 */
class SearchResultsResource extends ResourceBase {
  public function get($query) {
    $response = _hogwarts_search_completion_results($query);
    return new ResourceResponse($response);
  }
}
