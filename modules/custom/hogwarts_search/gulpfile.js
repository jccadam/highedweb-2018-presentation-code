const gulp = require('gulp');
const sass = require('gulp-sass');
const babelify = require('babelify');
const browserify = require('browserify');
const buffer = require('vinyl-buffer');
const source = require('vinyl-source-stream');

gulp.task('js', () => {
  const bundler = browserify('js/index.js');

  bundler.transform(babelify.configure({
    presets: ['@babel/preset-env', '@babel/preset-react'],
    plugins: [
      '@babel/plugin-proposal-class-properties',
      '@babel/plugin-proposal-object-rest-spread'
    ]
  }));

  return bundler.bundle()
    .on('error', err => console.error(err))
    .pipe(source('index.js'))
    .pipe(buffer())
    .pipe(gulp.dest('build/js'));
});

gulp.task('css', () => {
  return gulp.src('css/hogwarts_search.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('build/css'));
});

gulp.task('watch', () => {
  gulp.watch('js/**/*.js', ['js']);
  gulp.watch('css/**/*.scss', ['css']);
});
