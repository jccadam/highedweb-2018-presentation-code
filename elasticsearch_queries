GET /

GET _cat/indices




####################################
# Indices & Mapping
####################################


PUT heweb_hogwarts

GET heweb_hogwarts/_mapping

DELETE heweb_hogwarts

PUT heweb_hogwarts
{
  "mappings": {
    "node": {
      "properties": {
        "content_type": {
          "type": "text"
        },
        "title": {
          "type": "text",
          "analyzer": "english"
        },
        "body": {
          "type": "text",
          "analyzer": "english"
        }
      }
    }
  }
}

GET heweb_hogwarts/_analyze
{
  "field": "body",
  "text": "who drank all of the butter beers"
}




####################################
# Indexing
####################################


PUT heweb_hogwarts/node/1
{
  "content_type": "subject",
  "title": "potions",
  "body": "Potions is a core class and subject taught at Hogwarts. Horace Slughorn was the potionmaster for many years."
}

GET heweb_hogwarts/node/1

PUT heweb_hogwarts/node/2
{
  "content_type": "subject",
  "title": "charms",
  "body": "Charms is a core class and subject taught at Hogwarts. It is taught by Professor Flitwick."
}

PUT heweb_hogwarts/node/3
{
  "content_type": "subject",
  "title": "care of magical creatures",
  "body": "Care of Magical Creatures is an elective class at Hogwarts. Students learn about such creatures as flobberworms, fire crabs, and hippogriffs. Hagrid taught the subject for many years."
}

PUT heweb_hogwarts/node/4
{
  "content_type": "professor",
  "title": "rubeus hagrid",
  "body": "Rubeus Hagrid is a half-giant wizard. He serves as gamekeeper of Hogwarts."
}

PUT heweb_hogwarts/node/5
{
  "content_type": "professor",
  "title": "Dolores Umbridge",
  "body": "Dolores Umbridge was a Defense Against the Dark Arts professor at Hogwarts. She also served in the Ministry of Magic."
}




####################################
# Querying
####################################


GET heweb_hogwarts/node/3

GET heweb_hogwarts/_search?q=hagrid

GET heweb_hogwarts/_search
{
  "query": {
    "match_all": {}
  }
}

GET heweb_hogwarts/_search
{
  "query": {
    "multi_match": {
      "query": "magic",
      "fields": ["title", "body"]
    }
  }
}

GET heweb_hogwarts/_search
{
  "query": {
    "term": {
      "content_type": "professor"
    }
  }
}

GET heweb_hogwarts/_search
{
  "query": {
    "bool": {
      "must": {
        "match": {
          "body": "magic"
        }
      },
      "should": {
        "term": {
          "content_type": "subject"
        }
      },
      "must_not": {
        "match": {
          "title": "dolores"
        }
      }
    }
  }
}




####################################
# Completion Suggester
####################################


PUT heweb_hogwarts
{
  "mappings": {
    "node": {
      "properties": {
        "content_type": {
          "type": "text"
        },
        "title": {
          "type": "text",
          "analyzer": "english"
        },
        "body": {
          "type": "text",
          "analyzer": "english"
        },
        "suggest": {
          "type": "completion"
        }
      }
    }
  }
}

PUT heweb_hogwarts/node/1
{
  "content_type": "subject",
  "title": "potions",
  "body": "Potions is a core class and subject taught at Hogwarts. Horace Slughorn was the potionmaster for many years.",
  "suggest": ["potions", "horace", "slughorn"]
}

PUT heweb_hogwarts/node/2
{
  "content_type": "subject",
  "title": "charms",
  "body": "Charms is a core class and subject taught at Hogwarts. It is taught by Professor Flitwick.",
  "suggest": ["charms", "flitwick"]
}

PUT heweb_hogwarts/node/3
{
  "content_type": "subject",
  "title": "care of magical creatures",
  "body": "Care of Magical Creatures is an elective class at Hogwarts. Students learn about such creatures as flobberworms, fire crabs, and hippogriffs. Hagrid taught the subject for many years.",
  "suggest": ["care", "magical", "creatures"]
}

PUT heweb_hogwarts/node/4
{
  "content_type": "professor",
  "title": "rubeus hagrid",
  "body": "Rubeus Hagrid is a half-giant wizard. He serves as gamekeeper of Hogwarts.",
  "suggest": ["rubeus", "hagrid", "gamekeeper"]
}

PUT heweb_hogwarts/node/5
{
  "content_type": "professor",
  "title": "Dolores Umbridge",
  "body": "Dolores Umbridge was a Defense Against the Dark Arts professor at Hogwarts. She also served in the Ministry of Magic.",
  "suggest": ["dolores", "umbridge", "ministry", "magic", "dark", "arts"]
}

GET heweb_hogwarts/_search
{
  "suggest": {
    "node-suggest": {
      "prefix": "mag",
      "completion": {
        "field": "suggest"
      }
    }
  }
}
