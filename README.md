# HighEdWeb 2018

See modules/custom/hogwarts_search for all of the Elasticsearch, module, and UI
code.

elasticsearch_queries has all of the queries ran in Kibana during the
presentation.

If you want to try the module and/or play with the code:

* create an empty Drupal 8 site
* install the REST UI module
* copy the hogwarts_search folder to modules/custom and install it
* Go to Configuration->Web services->REST and enable Search Results Resource
  (Granularity=Resource, check Methods->GET, Formats->json,
  Authentication->cookie)
* Create some content types if you want, and add a basic text field called
  search_keywords

If you want to tinker with the UI code, make sure you have node and gulp
installed.

cd to the hogwarts_search module directory and run `npm install`.

Next, run `gulp watch` and hack away.